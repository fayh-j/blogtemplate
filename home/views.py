from django.shortcuts import render
from django.http import  HttpResponse, HttpResponseRedirect, JsonResponse
from django.core import serializers
from home.models import *
from django.utils.translation import *
from home.seeder import *

# Create your views here.
def populate(request):
	initSeed()
	try:
		if request.user.is_authenticated() and request.user.is_superuser(): initSeed()
	except: return HttpResponse('Not authenticated')



def home(request):
	admin, banner, about_st, service_st, work_st, abouts, services, works  = None,None,None,None, None, None, None, None

	try:
		admin = Admin.objects.all()[0]
		testimonials = Testimonial.objects.all()

		banner = Banner.objects.all()[0]

		about_st = Section_type.objects.filter( name='A propos' )[0]
		service_st = Section_type.objects.filter( name='Services')[0]
		work_st = Section_type.objects.filter( name='Traveaux réalisés' )[0]
		
		abouts = Section.objects.filter( section_type = Section_type.objects.get(name='A propos') )
		services = Section.objects.filter( section_type = Section_type.objects.get(name='Services') )
		works = Section.objects.filter( section_type = Section_type.objects.get(name='Traveaux réalisés') )
		
		return render(request, 'index.html', locals())
	except:
		pass
	return render(request, 'index.html', locals())
	#return HttpResponse()

