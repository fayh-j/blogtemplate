#python3 manage.py shell < home/initSeed.py
from home.models import *
from django.contrib.auth.models import User

if 1==1:
	#create Superuser
	User.objects.create_superuser('zimeoo', '', 'zimeooPwd')
	#create admin record
	Admin(logo='/static/images/logo.svg', mail='portofio@zimeoo.com', phone1='+41524204242', phone2='+41524204242', adresse1='Dubai, 06 PK9, Laguna',   ).save()

	#create banner
	Banner(title="Bienvenu sur mon portfolio", subtitle='Je suis un pro', image='/static/images/hero-bg.jpg').save()

	#create sections types
	section_types= ['A propos', 'Services', 'Traveaux réalisés']
	for sc in section_types:
		Section_type(name=sc, title='We have everything you need.', subtitle='Lorem ipsum dolor sit amet. ' ).save()



	#create about elements
	aboutST = Section_type.objects.filter(name='A propos')[0]
	about_items = ['Define','Desing','Build','Lauch']
	for it in about_items:
		Section(title=it, subtitle='Quos dolores saepe mollitia deserunt accusamus autem reprehenderit. Voluptas facere animi explicabo non quis magni recusandae. Numquam debitis pariatur omnis facere unde. Laboriosam minus amet nesciunt est. Et saepe eos maxime tempore quasi deserunt ab. ', section_type = aboutST ).save()



	#create services elements
	serviceST = Section_type.objects.filter(name='Services')[0]

	service_items = ['Brand Identity','Illustration','Web Design','Product Strategy', 'UI/UX Design', 'Mobile Development']
	service_icons = ['icon-tv', 'icon-group','icon-earth', 'icon-cube', 'icon-window', 'icon-lego-block']
	for i in range(len(service_items)):
		Section(title=service_items[i] , subtitle='Quos dolores saepe mollitia deserunt accusamus autem reprehenderit. Voluptas facere animi explicabo non quis magni recusandae. Numquam debitis pariatur omnis facere unde. Laboriosam minus amet nesciunt est. Et saepe eos maxime tempore quasi deserunt ab. ', section_type = serviceST, icon=service_icons[i] ).save()



	#create works elements
	workST = Section_type.objects.filter(name='Traveaux réalisés')[0]

	work_titles = ['Lamp','Web Design','Salad brading', 'UI/UX Design', 'Mobile Development', 'branding']
	work_subtutitles = ['Brand Identity','Illustration','Web Design','Product Strategy', 'UI/UX Design', 'Mobile Development']
	work_images = ['images/portfolio/gallery/g-lamp.jpg', 'images/portfolio/gallery/g-salad.jpg','images/portfolio/gallery/g-woodcraft.jpg', 'images/portfolio/gallery/g-liberty.jpg', 'images/portfolio/gallery/g-fuji.jpg', 'images/portfolio/gallery/g-shutterbug.jpg']
	work_images_mini = ['/images/portfolio/lamp.jpg', 'images/portfolio/salad.jpg' , 'images/portfolio/woodcraft.jpg', 'images/portfolio/liberty.jpg', 'images/portfolio/fuji.jpg', 'images/portfolio/shutterbug.jpg']
	for i in range(len(work_titles)):
		Section(title=work_titles[i] , subtitle= work_subtutitles[i] , section_type = workST, button_link="#", description='Vero molestiae sed aut natus excepturi. Et tempora numquam. Temporibus iusto quo.Unde dolorem corrupti neque nisi.', image=work_images[i], image_mini = work_images_mini[i] ).save()


	#create works TESTii
	testi_name = ['Tim Cook','Sundar Pichai','Satya Nadella']
	testi_job = ['CEO, Apple','CEO, google','CEO, microsoft']
	testi_image = ['images/avatars/user-01.jpg', 'images/avatars/user-05.jpg', 'images/avatars/user-02.jpg']
	
	for i in range(len(testi_name)):
		Testimonial(name=testi_name[i] , description= 'Qui ipsam temporibus quisquam velMaiores eos cumque distinctio nam accusantium ipsum. Laudantium quia consequatur molestias delectus culpa facere hic dolores aperiam. Accusantium quos qui praesentium corpori', button_link="#", image=testi_image[i], job = testi_job[i] ).save()

