from django.db import models
from django.utils import timezone
#from phonenumber_field.modelfields import PhoneNumberField

#from djrichtextfield.models import RichTextField
#from colorfield.fields import ColorField

# Create your models here.

from .utils_functions import *
from tinymce.models import HTMLField
from ckeditor.fields import RichTextField
#from djrichtextfield.models import RichTextField

"""class Adresse(models.Model):
	nom = models.CharField(max_length=100, verbose_name="Rue" )
	street = models.CharField(max_length=100, verbose_name="Rue" )
class Theme(models.Model):
    name =  models.CharField(max_length=100, verbose_name="Nom du thème", default='Défaut',  unique=True)
    theme_file = models.CharField(max_length=100, default='main.css', verbose_name="Nom du fichier css", unique=True )
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
    class Meta:
        verbose_name = "Thème"

    def __str__(self):
        return self.name
"""


class Admin(models.Model):
    firstname = models.CharField(max_length=100, verbose_name="Prénoms", unique=True )
    lastname = models.CharField( max_length=500, verbose_name="Nom", unique=True)
    logo = models.ImageField(upload_to='images/', null=True, blank=True, verbose_name="Logo du site")
    phone1 = models.IntegerField( help_text='Numéro de téléphone whatsapp', verbose_name="Numéro de téléphone whatsapp", unique=True)
    #phone2 = PhoneNumberField(null=True, blank=True, help_text='Numéro de téléphone 2', verbose_name="Numéro de téléphone 2", unique=True)
    mail = models.EmailField(max_length=50, verbose_name="Adresse mail", unique=True)
    facebook_link = models.CharField(max_length=500, null=True, blank=True, verbose_name="Lien facebook")
    twitter_link = models.CharField(max_length=500, default="#", null=True, blank=True,  verbose_name="Lien twitter")
    linkedin_link = models.CharField(max_length=500, default="#", null=True, blank=True,  verbose_name="Lien linkedin")
    insagram_link = models.CharField(max_length=500, default="#", null=True,  blank=True, verbose_name="Lien insagram")
    dribbble_link = models.CharField(max_length=500, default="#", null=True,  blank=True, verbose_name="Lien dribble")
    behance_link = models.CharField(max_length=500, default="#", null=True,  blank=True, verbose_name="Lien dehance")
    adresse1 = models.TextField(null=True, blank=True , default="#", verbose_name="Adresse principale")
    adresse2 = models.TextField(  null=True, blank=True,  verbose_name="Autre adresse ")
    #theme = models.OneToOneField(Theme, null=True, on_delete = models.PROTECT,  verbose_name="Thème de mon site ")
    photo1 = models.ImageField(upload_to='images/',null=True, blank=True, verbose_name="Ma photo")
    photo2 = models.ImageField(upload_to='images/', null=True, blank=True, verbose_name="Autre photo")
    photo3 = models.ImageField(upload_to='images/', null=True, blank=True, verbose_name="Autre photo 2")
    photo4 = models.ImageField(upload_to='images/', null=True, blank=True, verbose_name="Autre photo 3")
    #content = HTMLField( null = True )
    project_number = models.IntegerField(default=0, verbose_name="Nombre de projets réalisés")
    award_number = models.IntegerField(default=0, verbose_name="Nombre de prix")
    cup_number = models.IntegerField(default=0, verbose_name="Nombre de Réussites")
    customers_number = models.IntegerField(default=0, verbose_name="Nombre de Clients ")
    happy_customers_number = models.IntegerField(default=0, verbose_name="Nombre de  Clients satisfaits")
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

    """
    user = models.ForeignKey(User, null=True, on_delete = models.PROTECT)
    created_at = models.DateTimeField(default=timezone.now,   verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now,  verbose_name="Date de modification")
    phone = models.IntegerField(default=0)
    user = models.OneToOneField(User, null=True, on_delete = models.PROTECT)
    description = models.TextField(default="Un modèle responsif qui vous correspond")
    """

    class Meta:
        verbose_name = "Mes donnée"
        #ordering = ['name']

    def __str__(self):
        return "Mes données"


class Section_type(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom du type de section" )
    title = models.TextField( verbose_name="Titre" , null=True,  blank=True)
    subtitle = models.TextField( verbose_name="Sous titre", null=True,  blank=True,  )
    image = models.ImageField(upload_to='images/', null=True,  blank=True, verbose_name="Image du type de section")
    icon = models.CharField(blank=True, max_length=1000, verbose_name="Icon",  )
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

    class Meta:
        verbose_name = "Types de section"
        ordering = ['name']

    def __str__(self):
        return self.name




#section_type = models.ForeignKey(Section_type, on_delete = models.PROTECT)

class Section(models.Model):
    section_type = models.ForeignKey(Section_type, on_delete = models.PROTECT)
    title = models.TextField( verbose_name="Titre",)
    subtitle = models.TextField( verbose_name="Sous titre",  )
    description = models.CharField(null=True, blank=True, max_length=1000, verbose_name="Description", default=""  )
    buton_text = models.CharField(default="Voir", max_length=1000, verbose_name="Bouton voir",   )
    button_link = models.CharField(blank=True, max_length=1000, verbose_name="Lien vers la source",  )
    image = models.ImageField(upload_to='images/', null=True,  blank=True, verbose_name="Image de la section")
    image_mini = models.ImageField(upload_to='images/', null=True,  blank=True, verbose_name="Image miniature (Vous pouvez prendre la même image que celle de la section)")
    icon = models.CharField(blank=True, max_length=1000, verbose_name="Icone",  )
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

    class Meta:
        verbose_name = "Section"
        ordering = ['section_type']

    def __str__(self):
        return self.section_type.name+',  '+ self.title



class Banner(models.Model):
    title = models.TextField( verbose_name="Titre" )
    subtitle = models.TextField( verbose_name="Sous titre" )
    description = models.CharField(null=True, blank=True,max_length=1000, verbose_name="Description", default=""  )
    button_text = models.CharField(default="Commencer", max_length=1000, verbose_name="Bouton accueil",   )
    button_text2 = models.CharField(default="Commencer", max_length=1000, verbose_name="Autre bouton accueil",   )
    image = models.ImageField(upload_to='images/', null=True,  blank=True, verbose_name="Image de la bannière")
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

    class Meta:
        verbose_name = "Arrière plan principale"
        ordering = ['title']

    def __str__(self):
        return self.title


class Testimonial(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom et Prénoms de celui qui témoigne",  null=True, blank=True, )
    description = models.CharField(null=True, blank=True,max_length=1000, verbose_name="Description du Témoignage", default=""  )
    job = models.CharField(null=True, blank=True,max_length=1000, verbose_name="Description (Métier ou  fonction, etc)", default=""  )
    button_link = models.CharField(default="Voir", max_length=1000, verbose_name="Bouton vers la page du témoigneur",   )
    image = models.ImageField(upload_to='images/', null=True,  blank=True, verbose_name="Image du témoigneur")
    icon = models.CharField(blank=True, max_length=1000, verbose_name="Icon",  )
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

    class Meta:
        verbose_name = "Témoignage"
        #ordering = ['title']

    def __str__(self):
        return self.firstname




class Partner(models.Model):
	name = models.CharField(max_length=100, verbose_name="Nom du partenaire" )
	image = models.ImageField(upload_to='images/', null=True,  blank=True, verbose_name="Logo/Image du partenaire")
	created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
	updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

	class Meta:
		verbose_name = "Partenaire"

	def __str__(self):
		return self.name

class Team_member(models.Model):
	firstname = models.CharField(max_length=100, verbose_name="Prénoms" )
	lastname = models.CharField( max_length=500, verbose_name="Nom")
	#phone1 = PhoneNumberField( help_text='Numéro de téléphone 1', verbose_name="Numéro de téléphone")
	post = models.CharField( max_length=500, verbose_name="Nom")
	image = models.ImageField(upload_to='images/', null=True, verbose_name="Image du membre d'équipe")
	created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
	updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
	
	class Meta:
		verbose_name = "Membre d'équipe"

	def __str__(self):
		return self.firstname+' '+self.lastname


"""
class Color(models.Model):
    name = models.CharField(max_length=100, default="", null=True, blank=True, verbose_name="Nom de la couleur", unique=True)
    color = ColorField(default='#FF0000')
    theme = models.ForeignKey(Theme,  on_delete = models.PROTECT)
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    class Meta:
        verbose_name = "Couleur"
    def __str__(self):
        return self.name
"""
#95 45 90 91

class Author(models.Model):
    firstname = models.CharField(max_length=100, verbose_name="Prénoms", unique=True )
    lastname = models.CharField( max_length=500, verbose_name="Nom", unique=True)
    mail = models.EmailField(max_length=50, verbose_name="Adresse mail", unique=True)
    class Meta:
        verbose_name = "Auteur"

    def __str__(self):
        return self.firstname+ ' '+ self.lastname+', '+self.mail

class Abonne(models.Model):
    firstname = models.CharField(max_length=100, verbose_name="Prénoms", unique=True )
    lastname = models.CharField( max_length=500, verbose_name="Nom", unique=True)
    mail = models.EmailField(max_length=50, verbose_name="Adresse mail", unique=True)
    class Meta:
        verbose_name = "Abonné"

    def __str__(self):
        return self.firstname+ ' '+ self.lastname+', '+self.mail


class Categorie(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom de la catégorie" )
    class Meta:
        verbose_name = "Catégorie"

    def __str__(self):
        return self.name

class Article(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom de l'article" )
    #content = HTMLField( null = True, blank=True, verbose_name="Contenu de l'article" )
    content = RichTextField(config_name='default', null = True, blank=True, verbose_name="Contenu de l'article" )
    #content = models.TextField(max_length=100, null=True, blank=True, verbose_name="Contenu de l'article" )
    auteur = models.ForeignKey(Author, null=True, on_delete = models.PROTECT)
    categorie = models.ForeignKey(Categorie, null=True, on_delete = models.PROTECT)
    class Meta:
        verbose_name = "Article"

    def __str__(self):
        return self.name

class Like(models.Model):
    article = models.ForeignKey(Article, null=True, on_delete = models.PROTECT)
    abonne = models.ForeignKey(Abonne, null=True, on_delete = models.PROTECT)
    class Meta:
        verbose_name = "Like"

    def __str__(self):
        return self.article.name+' '+ self.abonne.firstname+' '+ self.abonne.lastname

class Tag(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom du tag" )
    class Meta:
        verbose_name = "Tag"

    def __str__(self):
        return self.name

class Article_tags(models.Model):
    article = models.ForeignKey(Article, null=True, on_delete = models.PROTECT)
    tag = models.ForeignKey(Tag, null=True, on_delete = models.PROTECT)
    class Meta:
        verbose_name = "Tags des article"

    def __str__(self):
        return self.article.name+' '+ self.tag.name

class Comment(models.Model):
    contenu = models.CharField(max_length=100, verbose_name="Contenu du commentaire" )
    article = models.ForeignKey(Article, null=True, on_delete = models.PROTECT)
    abonne = models.ForeignKey(Abonne, null=True, on_delete = models.PROTECT)
    class Meta:
        verbose_name = "Commentaires"

    def __str__(self):
        return self.abonne.firstname+ ' '+self.abonne.lastname+', '+ self.article.name
