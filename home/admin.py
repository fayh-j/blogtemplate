from django.contrib import admin
from home.models import *
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django import template
from django.template.defaultfilters import stringfilter
 

admin.site.index_title = _('Ziméoo, Administration de site')
admin.site.site_header = _('Administration de mon site Ziméoo')
admin.site.site_title = _('Gestion du contenu')

class AdminClass(admin.ModelAdmin):
    exclude = ['created_at','updated_at',]



admin.site.register(Admin, AdminClass)


#admin.site.register(Admin)
#admin.site.register(Testimonial)
#admin.site.register(Team_member)
#admin.site.register(Partner)
admin.site.register(Section)
#admin.site.register(Banner)
admin.site.register(Section_type)
admin.site.register(Author)
admin.site.register(Abonne)
admin.site.register(Categorie)
admin.site.register(Article)
admin.site.register(Like)
admin.site.register(Tag)
admin.site.register(Article_tags)
admin.site.register(Comment)

