from django.contrib import admin
from django.urls import include, path
from home import  urls
from django.conf.urls.i18n import i18n_patterns


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = i18n_patterns(
    # ...
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
    path(r'^tinymce/', include('tinymce.urls')), 
    path(r'^ckeditor/', include('ckeditor_uploader.urls')),

    #path('djrichtextfield/', include('djrichtextfield.urls'))
	
    # If no prefix is given, use the default language
    prefix_default_language=False
)
