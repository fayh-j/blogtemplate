from django.db import models
from django.utils import timezone
"""
from phonenumber_field.modelfields import PhoneNumberField

from djrichtextfield.models import RichTextField
from colorfield.fields import ColorField
from tinymce.models import HTMLField
"""

class Image(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom de l'image" )
    file = models.ImageField(upload_to='images/', null=True,  blank=True, verbose_name="Image")
    class Meta:
        verbose_name = "Image"

    def __str__(self):
        return self.file

class Video(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom de la vidéo" )
    file = models.ImageField(upload_to='videos/', null=True,  blank=True, verbose_name="Video")
    class Meta:
        verbose_name = "Video"

    def __str__(self):
        return self.file


class Audio(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom de l'audio" )
    file = models.ImageField(upload_to='audios/', null=True,  blank=True, verbose_name="Audio")
    class Meta:
        verbose_name = "Audio"

    def __str__(self):
        return self.file

class Document(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom du document" )
    file = models.ImageField(upload_to='Document/', null=True,  blank=True, verbose_name="Document")
    class Meta:
        verbose_name = "Document"

    def __str__(self):
        return self.file
