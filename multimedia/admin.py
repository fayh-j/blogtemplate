from django.contrib import admin
from multimedia.models import *
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django import template
from django.template.defaultfilters import stringfilter
 

admin.site.index_title = _('Ziméoo, Administration de site')
admin.site.site_header = _('Administration de mon site Ziméoo')
admin.site.site_title = _('Gestion du contenu')

class AdminClass(admin.ModelAdmin):
    exclude = ['created_at','updated_at',]



#admin.site.register(Admin, AdminClass)


admin.site.register(Audio)
admin.site.register(Video)
admin.site.register(Document)
admin.site.register(Image)

